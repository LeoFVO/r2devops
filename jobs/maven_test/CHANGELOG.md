# Changelog
All notable changes to this job will be documented in this file.

## [0.1.1] - 2022-03-24
* Upgrade image to 3.8.4-jdk-11

## [0.1.0] - 2021-04-18
* Initial version (renaming of `junit_test` job)
