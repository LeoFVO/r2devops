# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2021-10-20
* Updating aws CLI to v2
* Adding delete option
* Remove API config variables

## [0.1.0] - 2021-10-10
* Initial version
