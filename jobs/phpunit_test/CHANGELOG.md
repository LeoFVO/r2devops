# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2020-11-30
* New variable `PHPUNIT_MEMORY_LIMIT`, adds possibility to increase memory_limit of PHP

## [0.1.1] - 2020-11-16
* Fixing the installation of PHPUnit when it is not already available in the project

## [0.1.0] - 2020-10-28
* Initial version