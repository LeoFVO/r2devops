# Changelog
All notable changes to this job will be documented in this file.

## [0.3.0] - 2022-01-17
* Upgrade image `node` from `15.14` to `17-buster`

## [0.2.0] - 2021-04-23
* Upgrade image `node` from `15.7` to `15.14`

## [0.1.0] - 2021-04-07
* Initial version
