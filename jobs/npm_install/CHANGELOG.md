# Changelog
All notable changes to this job will be documented in this file.

## [0.3.1] - 2021-07-25
* Fix cache relative paths

## [0.3.0] - 2021-04-23
* Upgrade image `node` from `15.7` to `15.14`

## [0.2.0] - 2021-04-20
* Remove keyword `cache:when` for longer backward compatibility

## [0.1.1] - 2021-04-15
* Give the priority to `package-lock.json` to fetch package dependencies
* The possibility to use `npm ci` instead of `npm install`

## [0.1.0] - 2021-02-08
* Initial version
