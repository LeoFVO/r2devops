# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2021-04-20
* Update artifacts and add rules

## [0.1.1] - 2021-04-15
* Update job's stage

## [0.1.0] - 2021-04-13
* Initial version
