* Upgrade `lachlanevenson/k8s-helm` image version from `v3.0.2` to `v3.4.2`
* Upgrade `kubectl` version from `v1.17.0` to `v1.20.1`
* Upgrade `helm-secrets` version to `v3.4.0`
* Update `HELMSECRETS_URL` to the new repository `https://github.com/jkroepke/helm-secrets` because the old one is now [deprecated](https://github.com/zendesk/helm-secrets#deprecation-information)
* Update `STABLE_REPO_URL` default value to the new official address: `https://charts.helm.sh/stable`

