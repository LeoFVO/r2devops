# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2021-04-23
* Upgrade image `node` from `15.7` to `15.14`

## [0.1.2] - 2021-03-04
* Enable `artifact:expose_as` option to display job result in merge request

## [0.1.1] - 2021-02-24
* Fix Gulp version to gulp-cli@2.3.0

## [0.1.0] - 2021-02-17
* Initial version
