# Changelog
All notable changes to this job will be documented in this file.

## [0.1.2] - 2021-03-04
* Enable `artifact:expose_as` option to display job result in merge request

## [0.1.1] - 2020-12-07
* report added to zaproxy job, with different formats

## [0.1.0] - 2020-11-17
* Initial version